1 - Criar uma branch nova a partir da branch master clonada. Colocar seu nome no nome da branch

2 - Criar um arquivo de gitignore para ignorar uma pasta que já existirá com seu nome

3 - Criar um arquivo txt qualquer

4 - Efetuar o commit das suas alterações e enviar para o servidor. A mensagem do commit deve ser TESTE GIT SEU_NOME


5 - Quando chegarem nesse ponto me chamem no teamse aguardem para proseeguir


6 - Após o OK pra prosseguir, atualizar a sua branch com a versão mais recente das branch master, haverá itens novos e que vão gerar conflitos
(se não ocorrer o conflito tudo bem)

7 - Executar o comando para verificar a existência de novas branchs, haverá uma nova com o seu nome para usar

8 - Pegar as alterações dessa branch

9 - Fazer um novo commit de todas as alterações com a mensagem TESTE FINALIZADO

10 - Fazer um pull request para a branch master e me avisar